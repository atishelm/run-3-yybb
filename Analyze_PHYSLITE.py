"""
22 February 2023
Abraham Tishelman-Charny

The purpose of this module is to take basic looks at PHYSLITE files.
"""

# imports 
import uproot
import awkward as ak
from matplotlib import pyplot as plt
import numpy as np
from ROOT import TLorentzVector
import mplhep as hep 
import copy
plt.style.use(hep.style.ATLAS) # customization: https://mplhep.readthedocs.io/en/latest/api.html

# functions 
def Save_Figures(fig, fileTypes_, OUTNAME_, plotType):
	for fileType in fileTypes_:
			outname = "%s_%s.%s"%(OUTNAME_, plotType, fileType)
			print("")
			print("Saving figure:",outname)
			print("")
			fig.savefig(outname)
	plt.close()  

def Make_Histogram(yvals_, x_label_, ymaxIncrement, OUTNAME_):
  # histogram
	fileTypes = ["png", "pdf"]	
	Nentries = len(yvals_)

	fig, ax = plt.subplots()

	bin_dict = {
		"myy" : [100, 150, 35],
		#"myy" : [0, 180, 60],
		"N_photons" : [0, 10, 10],
		"pt" : [0, 200, 30],
		"eta" : [-3.2, 3.2, 30],
		"phi" : [-3.2, 3.2, 30]
	}

	# automatic binning
	#counts, bins = np.histogram(yvals_)

	# choose binning
	xmin, xmax, nbins = bin_dict[x_label_]
	bins = np.linspace(xmin, xmax, nbins+1)
	#bins = np.linspace(0,10,11)
	counts, bins = np.histogram(yvals_, bins=bins)

	avg, stdev = np.mean(yvals_), np.std(yvals_)
	ax.hist(bins[:-1], bins, weights=counts)
	hep.atlas.text("Preliminary")
	ax.set_xlabel(x_label_)
	ax.set_ylabel("Entries")

	ymin, ymax = ax.get_ylim()
	Incremented_Ymax = ymax + (ymax-ymin)*ymaxIncrement
	ax.set_ylim(ymin, Incremented_Ymax)

	# Upper left text 
	plt.text(
			0.06,
			0.95,
			"\n".join([
					"$\mu$ = %s" % float('%.4g' % avg),
					"$\sigma$ = %s" % float('%.4g' % stdev),
					"$N_{entries}$ = %s"%(Nentries)
			]
			),
			horizontalalignment='left',
			verticalalignment='top',
			fontweight = 'bold',
			transform=ax.transAxes
	)

	fig.tight_layout()
	Save_Figures(fig, fileTypes, OUTNAME_, "")

# PHYSLITE file downloaded from Rucio
inFile = "../yybb_PHYSLITE/mc20_13TeV.600022.PhPy8EG_PDF4LHC15_HHbbyy_cHHH10d0.deriv.DAOD_PHYSLITE.e8222_s3681_r13167_r13146_p5453_tid31885008_00/DAOD_PHYSLITE.31885008._000003.pool.root.1"

file = uproot.open(inFile)
tree = file["CollectionTree"]

photons = ak.zip(
    {
        "pt": tree["AnalysisPhotonsAuxDyn.pt"].array(),
        "eta": tree["AnalysisPhotonsAuxDyn.eta"].array(),
        "phi": tree["AnalysisPhotonsAuxDyn.phi"].array(),
        "m": tree["AnalysisPhotonsAuxDyn.m"].array(),
    },
    with_name="PhotonKinematics",
)

pt_ordered_photons = photons[ak.argsort(photons.pt, ascending=False)]

print("photons:",photons)
print("pt ordered:",pt_ordered_photons)

# can get lorenzvectors with SetPtEtaPhiM()
N_events = len(pt_ordered_photons)
N_photons = [len(i) for i in pt_ordered_photons]

selections = ["NoSelection", "AtLeast2Photons"]
#vars = ["pt", "eta", "myy"]
vars = ["myy"]
ol = "/eos/user/a/atishelm/www/yybb/PHYSLITE/" # output location for plots
for v in vars:
	for sel in selections:
		if(v=="myy"): 
			if(sel == "NoSelection"): continue
			else: 
				
				kins_strs = ["pt", "eta", "phi", "m"]
				pt = getattr(photons, "pt")
				eta = getattr(photons, "eta")
				phi = getattr(photons, "phi")
				m = getattr(photons, "m")
	
				kins = [pt, eta, phi, m]
				for kin_i, kin__ in enumerate(kins):
					kin = [i.tolist() for i in kin__ if len(i) >= 2]
					exec("%s = np.copy(kin)"%(kins_strs[kin_i]))

				vals = []
				N_events_ = len(pt)

				for i in range(0,N_events_):
					Loren_vec_lead = TLorentzVector()
					Loren_vec_sublead = TLorentzVector()
					Loren_vec_lead.SetPtEtaPhiM(pt[i][0], eta[i][0], phi[i][0], m[i][0])
					Loren_vec_sublead.SetPtEtaPhiM(pt[i][1], eta[i][1], phi[i][1], m[i][1])
					diphoton = Loren_vec_lead + Loren_vec_sublead
					diphoton_mass = diphoton.M() / 1000.  # GeV
					vals.append(diphoton_mass)

		else: all_vals = getattr(photons, v)
		if(v!="myy"): vals_ = [i.tolist() for i in all_vals]
		if(sel=="NoSelection"): vals = [item for sublist in vals_ for item in sublist]
		elif(sel=="AtLeast2Photons" and v!="myy"): vals = [item for sublist in vals_ for item in sublist if len(sublist) >= 2]
		if(v=="pt"): vals = [i/1000. for i in vals] # GeV
		Make_Histogram(vals, v, 0.5, "{}{}_photon_{}".format(ol, sel, v))

# Number of photons
Nentries = len(pt_ordered_photons)
y_label = "N_photon"
ymaxIncrement = 0.5
upperText = "" 
fileTypes = ["png", "pdf"]
OUTNAME = "/eos/user/a/atishelm/www/yybb/PHYSLITE/N_photons" # output location 

Make_Histogram(N_photons, "N_photons", 0.5, OUTNAME)
