# Run 3 yybb

The purpose of this repository is to store initial basic scripts for exploring Run 3 yybb analysis. This includes testing the feasibility of the PHYSLITE format for Run 3 yybb.

## Example usage 
 
```
git clone ssh://git@gitlab.cern.ch:7999/atishelm/run-3-yybb.git # via ssh
git clone https://gitlab.cern.ch/atishelm/run-3-yybb.git # via HTTPS
cd run-3-yybb
setupATLAS 
voms-proxy-init -voms atlas
lsetup rucio 
rucio ls mc20_13TeV:mc20_13TeV.60*.PhPy8EG_PDF4LHC15_HHbbyy*PHYS*
rucio download mc20_13TeV:mc20_13TeV.600022.PhPy8EG_PDF4LHC15_HHbbyy_cHHH10d0.deriv.DAOD_PHYSLITE.e8222_s3681_r13167_r13146_p5453_tid31885008_00
```

Then change input and output paths in `Analyze_PHYSLITE.py` module, and run:

```
python3 Analyze_PHYSLITE.py
```

## Extra information

Datasets:

```
mc20_13TeV.600021.PhPy8EG_PDF4LHC15_HHbbyy_cHHH01d0.recon.AOD.e8222_s3681_r13167
mc20_13TeV.600022.PhPy8EG_PDF4LHC15_HHbbyy_cHHH10d0.recon.AOD.e8222_s3681_r13167
```
